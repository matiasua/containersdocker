import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import marshmallow
from flask_marshmallow import Marshmallow
from flask_cors import CORS


db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()


def create_app():

    app = Flask(__name__)
    app_config = os.getenv("APP_CONFIG")
    app.config.from_object(app_config)

    CORS(app)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    config_Blueprint(app)
    error_handler(app)

    return app


def config_Blueprint(app):
    from Project.Users.endpoints import user_blueprint
    app.register_blueprint(user_blueprint)


def error_handler(app):
    @app.errorhandler(marshmallow.exceptions.ValidationError)
    def validationErrorHandler(ex):
        return ex.messages, 400
