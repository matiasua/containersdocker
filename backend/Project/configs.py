class ConfigDB:
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'dbpostgres',
        '5432',
        'backend'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(ConfigDB):
    SQLALCHEMY_ECHO = True


class ProductionConfig(ConfigDB):
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        '12345678',
        'database-1.cvbzfcyfnv9k.sa-east-1.rds.amazonaws.com',
        '5432',
        'backend'

    )
    SQLALCHEMY_ECHO = False


class TestingConfig(ConfigDB):
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'dbpostgres',
        '5432',
        'backend_test'
    )
    SQLALCHEMY_ECHO = False
