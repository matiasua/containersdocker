import os
import sys
import unittest
import coverage
from Project import create_app


COV = None

if os.environ.get('FLASK_ENV') == 'development':
    COV = coverage.coverage(
        branch=True,
        include='Project/*',
        omit=[
            'Project/__init__.py',
            'Project/Users/_testsUser/*',
            'Project/*/tests/*',
        ]
    )
    COV.start()


app = create_app()


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('Project', pattern='test_*.py')

    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        sys.exit(0)
    sys.exit(1)


@app.cli.command()
def cov():
    if COV is None:
        print('Ejecución de la cobertura en el entorno de producción')
        exit(0)
    tests = unittest.TestLoader().discover('Project', pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        COV.stop()
        COV.save()
        COV.report(show_missing=True)
        COV.html_report()
        COV.erase()
        sys.exit(0)
    sys.exit(1)
